package com.tanhua.server.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

@Data
public abstract class BasePojo {
    
//
//    fill    字段填充标记 （ FieldFill, 配合自动填充使用 ）
//    DEFAULT    默认不处理
//INSERT    插入填充字段
//UPDATE    更新填充字段
//INSERT_UPDATE    插入和更新填充字段
@TableField(fill = FieldFill.INSERT)
    private Date created;
@TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updated;
}
