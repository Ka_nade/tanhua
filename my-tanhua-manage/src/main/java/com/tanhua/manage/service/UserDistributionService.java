package com.tanhua.manage.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.manage.enums.AreaEnum;
import com.tanhua.manage.enums.SexEnum;
import com.tanhua.manage.mapper.UserDistributionMapper;
import com.tanhua.manage.pojo.UserDistribution;

import com.tanhua.manage.vo.DataPointVo;
import com.tanhua.manage.vo.UserDistributionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserDistributionService {
    @Autowired
    private UserDistributionMapper userDistributionMapper;

    private static final ObjectMapper MAPPER = new ObjectMapper();


    public UserDistributionVo queryDistribution(Long startDate,Long endDate) {

        UserDistributionVo userDistributionVo = new UserDistributionVo();
        userDistributionVo.setIndustryDistribution(queryIndustryDistribution(startDate,endDate));
        userDistributionVo.setAgeDistribution(queryAgeDistribution(startDate,endDate));
        userDistributionVo.setGenderDistribution(queryGenderDistribution(startDate,endDate));
        userDistributionVo.setLocalDistribution(queryLocalDistribution(startDate,endDate));
        userDistributionVo.setLocalTotal(querylocalTotal(startDate,endDate));

        return userDistributionVo;


    }

    /*
      查询行业分布TOP10
        */
    public List<DataPointVo> queryIndustryDistribution(Long startDate,Long endDate) {
        int i = 0;
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        String start = timeStampToTime(startDate);
        String end = timeStampToTime(endDate);
        queryWrapper.select("industry AS title, COUNT(*) AS amount").between("updated",start,end).groupBy("industry").orderByDesc("COUNT(*)");
        List<Map<String, Object>> maps = this.userDistributionMapper.selectMaps(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();

        for (Map<String, Object> map : maps) {
            DataPointVo dataPointVo = new DataPointVo();

            dataPointVo.setTitle((String) map.get("title"));
            dataPointVo.setAmount((Long) map.get("amount"));

            list.add(dataPointVo);
            i++;
            /*
            取10条数据
             */
            if (i == 9) {
                break;
            }
        }
        // userDistributionVo.setIndustryDistribution(list);
        return list;
    }

    /*
        查询行业年龄分布
          */
    public List<DataPointVo> queryAgeDistribution(Long startDate,Long endDate) {
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        String start = timeStampToTime(startDate);
        String end = timeStampToTime(endDate);
        queryWrapper.select("age").between("updated",start,end);
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        List<DataPointVo> list = new ArrayList<>();

        Long num1 = 0L;//0-17岁
        Long num2 = 0L;//18-23岁
        Long num3 = 0L;//24-30岁
        Long num4 = 0L;//31-40岁
        Long num5 = 0L;//41-50岁
        Long num6 = 0L;//50岁+


        for (UserDistribution userDistribution : userDistributions) {
            DataPointVo dataPointVo = new DataPointVo();
            int age = userDistribution.getAge();

            if (age >= 0 && age < 17) {
                num1++;
            } else if (age >= 18 && age < 23) {
                num2++;
            } else if (age >= 24 && age < 30) {
                num3++;
            } else if (age >= 31 && age < 40) {
                num4++;
            } else if (age >= 41 && age < 50) {
                num5++;
            } else if (age >= 50) {
                num6++;
            }

        }
        list.add(new DataPointVo("0-17岁", num1));
        list.add(new DataPointVo("18-23岁", num2));
        list.add(new DataPointVo("24-30岁", num3));
        list.add(new DataPointVo("31-40岁", num4));
        list.add(new DataPointVo("41-50岁", num5));
        list.add(new DataPointVo("50岁+", num6));
        // userDistributionVo.setIndustryDistribution(list);
        return list;
    }

    /*
     查询性别分布
       */
    public List<DataPointVo> queryGenderDistribution(Long startDate,Long endDate) {
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        String start = timeStampToTime(startDate);
        String end = timeStampToTime(endDate);
        queryWrapper.select("sex").between("updated",start,end);
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();

        Long man = 0L;//0-17岁
        Long woman = 0L;//18-23岁

        for (UserDistribution userDistribution : userDistributions) {
            int sex = userDistribution.getSex();

            if (sex == 1) {
                man++;
            } else if (sex == 2) {
                woman++;
            }

        }
        list.add(new DataPointVo(SexEnum.MAN.toString(), man));
        list.add(new DataPointVo(SexEnum.WOMAN.toString(), woman));


        return list;
    }

    /*
      查询地区分布

        */
    public List<DataPointVo> queryLocalDistribution(Long startDate,Long endDate) {
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        String start = timeStampToTime(startDate);
        String end = timeStampToTime(endDate);
        queryWrapper.select("city").between("updated",start,end);
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();
        Map<String, Integer> map = new HashMap<>();

//        上海,河北,山西,内蒙古,辽宁,吉林,黑龙江,江苏,
//                浙江,安徽,福建,江西,山东,河南,湖北,
//                湖南,广东,广西,海南,四川,贵州,云南,西藏,
//                陕西,甘肃,青海,宁夏,新疆,北京,天津,重庆,香港,澳门

        for (UserDistribution userDistribution : userDistributions) {
            String city = userDistribution.getCity();
            //将city分割获取省
            String[] split = city.split("-");
            String cityName = split[0];
            //判断当前map是否包含省，有则获取值并+1后重新赋值
            if (map.containsKey(cityName)) {
                Integer cityNum = map.get(cityName);
                map.put(cityName, cityNum + 1);
                //不包含则创建key为省名value为1的数据
            } else {
                map.put(cityName, 1);
            }
        }
        //遍历map，将数据写入list
        map.forEach((key, value) -> {
            list.add(new DataPointVo(key, value.longValue()));

        });
        return list;
    }


    /*
  查询地区合计

    */

    public List<DataPointVo> querylocalTotal(Long startDate,Long endDate){
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        String start = timeStampToTime(startDate);
        String end = timeStampToTime(endDate);
        queryWrapper.select("city").between("updated",start,end);
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();
        Map<String,Integer> map = new HashMap<>();

        for (UserDistribution userDistribution : userDistributions) {
            String city = userDistribution.getCity();
            //将city分割获取省
            String[] split = city.split("-");
            String cityName = split[0].substring(0,2);
            String area = AreaEnum.getAreaByProvince(cityName);

            //判断当前map是否包含省，有则获取值并+1后重新赋值
            if (map.containsKey(area)){
                Integer areaNum = map.get(area);
                map.put(area,areaNum+1);
                //不包含则创建key为省名value为1的数据
            }else {
                map.put(area,1);
            }
        }
        //遍历map，将数据写入list
        map.forEach((key, value) -> {
            list.add(new DataPointVo(key, value.longValue()));

        });
        return list;
    }
//时间戳转换日期
    public  String timeStampToTime(Long time) {
       // long time =1611557692L*1000;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(time*1000);
        String result = simpleDateFormat.format(date);
        return result;
    }

}
