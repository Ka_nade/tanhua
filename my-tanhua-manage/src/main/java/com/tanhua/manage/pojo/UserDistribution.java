package com.tanhua.manage.pojo;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.annotation.TableName;
import com.tanhua.manage.enums.SexEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_user_info")//指定表名
public class UserDistribution {

    /*
    行业 年龄 性别 地区分布 地区合计
     */

    private String industry;     //行业
    private int age ;            //年龄
    private Integer sex;         //性别 1 男 2女
    private String city;         //居住城市
    private Date updated;   //最后修改时间

}
