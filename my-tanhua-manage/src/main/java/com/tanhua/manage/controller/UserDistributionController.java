package com.tanhua.manage.controller;


import com.tanhua.manage.service.UserDistributionService;
import com.tanhua.manage.vo.UserDistributionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("dashboard")
public class UserDistributionController {
    @Autowired
    private UserDistributionService userDistributionService;

    @GetMapping("distribution")
    public UserDistributionVo sendMsg(@RequestParam("sd") Long startDate,
                                      @RequestParam("ed") Long endDate,
                                      @RequestHeader("Authorization") String token){

        UserDistributionVo userDistributionVo =
                userDistributionService.queryDistribution(startDate,endDate);

        // u.setIndustryDistribution();
        return userDistributionVo;
    }
}
