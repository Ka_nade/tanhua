package com.tanhua.manage.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.tanhua.manage.enums.SexEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDistributionVo {

    /*
    行业 年龄 性别 地区分布 地区合计
     */

//    private Object[]  industryDistribution;     //行业分布TOP10
//    private Object[]  ageDistribution;          //年龄分布
//    private Object[]  genderDistribution;       //性别分布
//    private Object[]  localDistribution;        //地区分布
//    private Object[]  localTotal;                //地区合计

    private  List<DataPointVo> industryDistribution;     //行业分布TOP10
    private  List<DataPointVo>  ageDistribution;          //年龄分布
    private  List<DataPointVo>  genderDistribution;       //性别分布
    private  List<DataPointVo>  localDistribution;        //地区分布
    private  List<DataPointVo>  localTotal;                //地区合计
}
