package com.tanhua.manage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tanhua.manage.enums.AreaEnum;
import com.tanhua.manage.enums.SexEnum;
import com.tanhua.manage.mapper.UserDistributionMapper;
import com.tanhua.manage.pojo.UserDistribution;

import com.tanhua.manage.vo.DataPointVo;
import com.tanhua.manage.vo.UserDistributionVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Date;


@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDistributionService {
    private static final ObjectMapper MAPPER = new ObjectMapper();
    @Autowired
    private UserDistributionMapper userDistributionMapper;

    private UserDistributionVo userDistributionVo;
    @Test
    public void queryUserDistribution(){
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("age",33);

        List<UserDistribution> userInfos = this.userDistributionMapper.selectList(queryWrapper);
        System.out.println(userInfos);

    }

    /*
   查询行业分布TOP10
     */
    @Test
    public void queryUserDistribution1(){
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("industry AS title, COUNT(*) AS amount").groupBy("industry").orderByDesc("COUNT(*)");
       List<Map<String, Object>> maps = this.userDistributionMapper.selectMaps(queryWrapper);
        StringWriter json = new StringWriter();
        try {
            MAPPER.writeValue(json,maps);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Object[] split = json.toString().split(",");
        for (int i = 0; i < split.length; i++) {
            System.out.println(split[i]);
        }
     //   System.out.println(split);
//        for (Map<String, Object> map : maps) {
//            System.out.println(map);
//        }

    }
    /*
 查询行业分布TOP10--2
   */
    @Test
    public void queryUserDistribution2(){
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("industry AS title, COUNT(*) AS amount").groupBy("industry").orderByDesc("COUNT(*)");
        List<Map<String, Object>> maps = this.userDistributionMapper.selectMaps(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();
        DataPointVo dataPointVo =new DataPointVo();
        for (Map<String, Object> map : maps) {
            System.out.println(map);
            dataPointVo.setTitle((String)map.get("title"));
            dataPointVo.setAmount((Long) map.get("amount"));
            list.add(dataPointVo);

        }
        userDistributionVo.setIndustryDistribution(list);
        System.out.println(userDistributionVo);
    }

    /*
      查询行业年龄分布
        */
    @Test
       /*
        查询行业年龄分布
          */
    public void queryAgeDistribution(){
        int i =0;
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("age");
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();

        Long num1=0L;//0-17岁
        Long num2=0L;//18-23岁
        Long num3=0L;//24-30岁
        Long num4=0L;//31-40岁
        Long num5=0L;//41-50岁
        Long num6=0L;//50岁+


        for (UserDistribution userDistribution : userDistributions) {
            DataPointVo dataPointVo =new DataPointVo();
            int age = userDistribution.getAge();

            if (age>=0&&age<17){
                num1++;
            }else if (age>=18&&age<23){
                num2++;
            }else if (age>=24&&age<30){
                num3++;
            }else if (age>=31&&age<40){
                num4++;
            }else if (age>=41&&age<50){
                num5++;
            }else if (age>=50){
                num6++;
            }

        }
        list.add(new DataPointVo("0-17岁",num1));
        list.add(new DataPointVo("18-23岁",num2));
        list.add(new DataPointVo("24-30岁",num3));
        list.add(new DataPointVo("31-40岁",num4));
        list.add(new DataPointVo("41-50岁",num5));
        list.add(new DataPointVo("50岁+",num6));

        userDistributionVo.setAgeDistribution(list);
        System.out.println(userDistributionVo);
    }
    @Test
       /*
        查询性别分布
          */
    public void queryGenderDistribution(){
        int i =0;
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("sex");
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();

        Long man=0L;//0-17岁
        Long woman=0L;//18-23岁

        for (UserDistribution userDistribution : userDistributions) {
            int sex = userDistribution.getSex();

            if (sex==1){
                man++;
            }else if (sex==2){
                woman++;
            }

        }
        list.add(new DataPointVo(SexEnum.MAN.toString(),man));
        list.add(new DataPointVo(SexEnum.WOMAN.toString(),woman));

        userDistributionVo.setGenderDistribution(list);
        System.out.println(userDistributionVo);
    }

    @Test
       /*
        查询地区分布

          */
    public void queryLocalDistribution(){
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("city");
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();
        Map<String,Integer> map = new HashMap<>();

//        上海,河北,山西,内蒙古,辽宁,吉林,黑龙江,江苏,
//                浙江,安徽,福建,江西,山东,河南,湖北,
//                湖南,广东,广西,海南,四川,贵州,云南,西藏,
//                陕西,甘肃,青海,宁夏,新疆,北京,天津,重庆,香港,澳门

        for (UserDistribution userDistribution : userDistributions) {
            String city = userDistribution.getCity();
            //将city分割获取省
            String[] split = city.split("-");
            String cityName = split[0];
            //判断当前map是否包含省，有则获取值并+1后重新赋值
            if (map.containsKey(cityName)){
                Integer cityNum = map.get(cityName);
                map.put(cityName,cityNum+1);
            //不包含则创建key为省名value为1的数据
            }else {
                map.put(cityName,1);
            }
        }
        //遍历map，将数据写入list
        map.forEach((key, value) -> {
            list.add(new DataPointVo(key, value.longValue()));

        });


        userDistributionVo.setGenderDistribution(list);
        System.out.println(userDistributionVo);



    }

    /*
  查询地区分布

    */
    @Test
    public void querylocalTotal(){
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("city");
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        UserDistributionVo userDistributionVo = new UserDistributionVo();
        List<DataPointVo> list = new ArrayList<>();
        Map<String,Integer> map = new HashMap<>();

        for (UserDistribution userDistribution : userDistributions) {
            String city = userDistribution.getCity();
            //将city分割获取省
            String[] split = city.split("-");
            String cityName = split[0].substring(0,2);
            String area = AreaEnum.getAreaByProvince(cityName);

            //判断当前map是否包含省，有则获取值并+1后重新赋值
            if (map.containsKey(area)){
                Integer areaNum = map.get(area);
                map.put(area,areaNum+1);
                //不包含则创建key为省名value为1的数据
            }else {
                map.put(area,1);
            }
        }
        //遍历map，将数据写入list
        map.forEach((key, value) -> {
            list.add(new DataPointVo(key, value.longValue()));

        });


        userDistributionVo.setLocalTotal(list);
        System.out.println(userDistributionVo);



    }
    @Test
    public void testE(){
        QueryWrapper<UserDistribution> queryWrapper = new QueryWrapper<>();
        String date1 ="2000-09-18 ";
        String date2="2020-09-18 ";
        queryWrapper.select("*").between("updated",date1,date2);
        List<UserDistribution> userDistributions = this.userDistributionMapper.selectList(queryWrapper);
        System.out.println(userDistributions);


    }
    @Test
    /**
     * 时间戳转换成时间
     * @param s 传入的时间戳
     * @return 返回格式化时间
     */
    public  void timeStampToTime() {
        Long time =1611557692L*1000;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(time);
       String da = simpleDateFormat.format(date);
        System.out.println(da);
    }
    }
